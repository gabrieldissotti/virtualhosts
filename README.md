# Virtual Host *Manager*

>  For Linux (*Debian Distros*)

This is a utility to web developers manage vhosts. With this tool we can create custom URLs for our sites or web applications.
**Do not forget to star!**

## How to install Virtual Host *Manager* v1.0

- Download DEB from [official repository](https://drive.google.com/open?id=1PBF6NAmu_StviKQf6kBBdpnUff6ogrPG)

- Use this command in your terminal:

> cd ~/Downloads && sudo dpkg -i vhost-manager_1.0_all.deb

## How to Use
- Use this command in your terminal to open Virtual Host Manager:

> vhost

## How to Uninstall

 - Use this command in your terminal:

> sudo dpkg -r vhost-manager
